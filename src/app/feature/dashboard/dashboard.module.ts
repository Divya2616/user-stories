import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { UserStoriesComponent } from './user-stories/user-stories.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
    path: '', component: DashboardComponent, children: [
      { path: '', redirectTo: 'user-stories', pathMatch: 'full' },
      { path: 'user-stories', component: UserStoriesComponent },
    ]
  }
];

@NgModule({
  declarations: [
    DashboardComponent,
    UserStoriesComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class DashboardModule { }
