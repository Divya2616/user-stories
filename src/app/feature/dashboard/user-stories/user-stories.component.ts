import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/core/global.service';

@Component({
  selector: 'app-user-stories',
  templateUrl: './user-stories.component.html',
  styleUrls: ['./user-stories.component.css']
})
export class UserStoriesComponent implements OnInit {
  users: any;
  details: any;
  userDetails: any;

  constructor(private _globalSrvice: GlobalService) { }

  ngOnInit(): void {
    this.getTabData();
    this.emittedDataByChild(1);
  }

  getTabData() {
    this._globalSrvice.fetchData("assets/json/user.json").subscribe((res: any) => {
      if (res) {
        this.users = res;
      }
    })
  }


  emittedDataByChild(data:any){
    this._globalSrvice.fetchData("assets/json/details.json").subscribe((res: any) => {
      if (res) {
        res.forEach((item: any) => {
          if (item.id == data) {
            this.details = item;
            console.log(this.details);
            
          } 
        })
      }
    })
  }

}
