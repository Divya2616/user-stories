import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-card-status',
  templateUrl: './card-status.component.html',
  styleUrls: ['./card-status.component.css']
})
export class CardStatusComponent implements OnInit {

  @Input('details') details: any;

  constructor() { }

  ngOnInit(): void {
  }

}
