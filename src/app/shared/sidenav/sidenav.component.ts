import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit, AfterViewInit {

  @ViewChild('mySidebar', { static: false }) mySidebar !: ElementRef;
  @ViewChild('main', { static: false }) main !: ElementRef;

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.openSmallNav();
  }

  openSmallNav() {
    this.mySidebar.nativeElement.style.width = "90px";
    this.main.nativeElement.style.marginLeft = "90px";
  }
  openFullNav() {
    this.mySidebar.nativeElement.style.width = "250px";
    this.main.nativeElement.style.marginLeft = "250px";
  }

}
