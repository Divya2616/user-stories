import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-card-tabs',
  templateUrl: './card-tabs.component.html',
  styleUrls: ['./card-tabs.component.css']
})
export class CardTabsComponent implements OnInit {

  @Input('users') users: any;
  @Output() emitData = new EventEmitter<string>();
  selectedItem: any;


  constructor() { }

  ngOnInit(): void {
  }

  showdetails(id: any,index:any) {
    this.emitData.emit(id);
  }
 
  listClick(event:any, newValue:any) {
    console.log(newValue);
    this.selectedItem = newValue;  // don't forget to update the model here
    // ... do other stuff here ...
}

}
