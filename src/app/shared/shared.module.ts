import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidenavComponent } from './sidenav/sidenav.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CardTabsComponent } from './card-tabs/card-tabs.component';
import { TableComponent } from './table/table.component';
import { CardStatusComponent } from './card-status/card-status.component';
import { HeaderComponent } from './header/header.component';

@NgModule({
  declarations: [
    SidenavComponent,
    CardTabsComponent,
    TableComponent,
    CardStatusComponent,
    HeaderComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule
  ],
  exports: [
    SidenavComponent,
    CardTabsComponent,
    TableComponent,
    CardStatusComponent,
    HeaderComponent
  ]
})
export class SharedModule { }
