import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class GlobalService {

  constructor(private http: HttpClient) { }

  fetchData(url: any): Observable<any> {
    return this.http.get(url)
  }
}
